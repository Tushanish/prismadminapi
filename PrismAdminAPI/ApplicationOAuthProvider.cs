﻿using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http.Cors;
using PrismAdminAPI.Models;
using System;

namespace PrismAdminAPI
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ApplicationOAuthProvider : OAuthAuthorizationServerProvider
    {
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            var identity = new ClaimsIdentity(context.Options.AuthenticationType);
            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });

            var currentUser = new Login() { FirstName="Tushar",LastName= "Harlikar",PhoneNumber="9763968153",LoginId=1001,Password="prism@123" };

            if (currentUser != null && context.UserName == currentUser.PhoneNumber && context.Password == currentUser.Password)
            {
                identity.AddClaim(new Claim("Id", Convert.ToString(currentUser.LoginId)));

                var props = new AuthenticationProperties(new Dictionary<string, string>
                                    {
                                        {
                                        "Name", $"{currentUser.FirstName} {currentUser.LastName}"
                                        },
                                        {
                                        "PhoneNumber", currentUser.PhoneNumber
                                        },
                                        {
                                        "LoginId", currentUser.LoginId.ToString()
                                        }
                                    });
                var ticket = new AuthenticationTicket(identity, props);
                context.Validated(ticket);
            }
            else
            {
                context.SetError("invalid_grant", "Provided username and password is not matching, Please retry.");
                context.Rejected();
            }
        }
    }
}