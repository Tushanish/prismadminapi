﻿namespace PrismAdminAPI.ViewModel
{
    public class SubjectViewModel
    {
        public long SubjectId { get; set; }
        public string SubjectName { get; set; }
        public string Description { get; set; }
        public bool IsSubjectActive { get; set; }
        public long MediumId { get; set; }
        public string MediumName { get; set; }
        public bool IsSubjectMediumActive { get; set; }
        public long StandardId { get; set; }
        public string StandardName { get; set; }
        public byte[] SubjectThumbnail { get; set; }
    }
}