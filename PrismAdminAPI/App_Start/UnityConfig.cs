using System.Web.Mvc;
using Unity;
using Unity.Mvc5;
using PrismAdminAPI.Manager;
using PrismAdminAPI.Repository;

namespace PrismAdminAPI
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();

            container.RegisterType<IStandardRepository, StandardRepository>();
            container.RegisterType<IStandardManager, StandardManager>();

            container.RegisterType<IMediumRepository, MediumRepository>();
            container.RegisterType<IMediumManager, MediumManager>();

            container.RegisterType<ISectionRepository, SectionRepository>();
            container.RegisterType<ISectionManager, SectionManager>();

            container.RegisterType<ISubjectManager, SubjectManager>();
            container.RegisterType<ISubjectRepository, SubjectRepository>();

            container.RegisterType<IConfigurationRepository, ConfigurationRepository>();
            container.RegisterType<IConfigurationManager, ConfigurationManager>();

            container.RegisterType<IChapterRepository, ChapterRepository>();
            container.RegisterType<IChapterManager, ChapterManager>();

            container.RegisterType<ICouponRepository, CouponRepository>();
            container.RegisterType<ICouponManager, CouponManager>();

            container.RegisterType<ISponsorerRepository, SponsorerRepository>();
            container.RegisterType<ISponsorerManager, SponsorerManager>();

            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }
    }
}