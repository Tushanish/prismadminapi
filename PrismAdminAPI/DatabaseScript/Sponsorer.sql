﻿/****** Object:  Table [dbo].[Mas_Sponsorer]    Script Date: 3/7/2020 9:55:31 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Mas_Sponsorer](
	[SponsorerId] [bigint] IDENTITY(1,1) NOT NULL,
	[SponsorerName] [nvarchar](max) NOT NULL,
	[ThumbnailImage] [varbinary](max) NULL,
	[LandingImage] [varbinary](max) NULL,
	[IsActive] [bit] NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL CONSTRAINT [DF_Mas_Sponsorer_CreatedDate]  DEFAULT (getdate()),
	[ModifiedBy] [nvarchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_Mas_Sponsorer] PRIMARY KEY CLUSTERED 
(
	[SponsorerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

SET ANSI_PADDING OFF
GO


/****** Object:  StoredProcedure [dbo].[uspGetAllSponsorer]    Script Date: 3/8/2020 6:43:08 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Anish.Bapat>
-- Create date: <07-03-2020>
-- Description:	<Retrive all Subject Information>
-- =============================================
CREATE PROCEDURE [dbo].[uspGetAllSponsorer] 
	@SponsorerId bigint=null,
	@IsActive bit=null

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
   
	SELECT MSP.SponsorerId,MSP.SponsorerName,MSP.ThumbnailImage AS ThumbnailImageByteData ,MSP.LandingImage AS LandingImageByteData,MSP.IsActive,MSP.CreatedDate,MSP.CreatedBy FROM Mas_Sponsorer MSP
	WHERE (ISNULL(@SponsorerId,0)=0 OR MSP.SponsorerId=@SponsorerId) AND (ISNULL(@IsActive,0)=0 OR MSP.IsActive=@IsActive)
END
GO

/****** Object:  StoredProcedure [dbo].[uspActiveDeactivateSponsorer]    Script Date: 3/8/2020 6:49:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Anish.Bapat>
-- Create date: <07-03-2020>
-- Description:	<Acitvate Deactivate Sponsorer>
-- =============================================
CREATE PROCEDURE [dbo].[uspActiveDeactivateSponsorer]
	@SponsorerId bigint,
	@IsActive bit,
	@RowCount int OUT
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRANSACTION 
	BEGIN TRY
       UPDATE Mas_Sponsorer SET 
	   IsActive=@IsActive
	   WHERE SponsorerId=@SponsorerId
	   SET @RowCount= @@RowCount
	   COMMIT TRANSACTION ;
    END TRY
    BEGIN CATCH
		INSERT INTO Mas_ErrorLogs(ErrorProcedure,ErrorLine,ErrorMessage)
		VALUES(ERROR_PROCEDURE(),ERROR_LINE(),ERROR_MESSAGE())
		SET @RowCount=0
		ROLLBACK TRANSACTION ;
    END CATCH
END
GO

/****** Object:  StoredProcedure [dbo].[uspInsertSponsorer]    Script Date: 3/8/2020 6:50:19 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Anish.Bapat>
-- Create date: <07-03-2020>
-- Description:	<Insert Sponsorer Details>
-- =============================================
CREATE PROCEDURE [dbo].[uspInsertSponsorer]
	@SponsorerId bigint,
	@SponsorerName nvarchar(200),
	@ThumbnailImage varbinary(MAX)=null,
	@LandingImage varbinary(MAX)=null,
	@IsActive bit = null,
	@CreatedBy bigint = null,
	@ModifiedBy bigint = null,
	@ReturnId bigint OUT
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRANSACTION 
	BEGIN TRY
       INSERT INTO  Mas_Sponsorer(SponsorerName,ThumbnailImage,LandingImage,IsActive,CreatedBy,ModifiedBy) 
	   VALUES (@SponsorerName,@ThumbnailImage,@LandingImage,@IsActive,@CreatedBy,@ModifiedBy)
	   
	   SET @ReturnId =SCOPE_IDENTITY()
	   COMMIT TRANSACTION ;
    END TRY
    BEGIN CATCH
		INSERT INTO Mas_ErrorLogs(ErrorProcedure,ErrorLine,ErrorMessage)
		VALUES(ERROR_PROCEDURE(),ERROR_LINE(),ERROR_MESSAGE())
		SET @ReturnId =0
		ROLLBACK TRANSACTION ;
    END CATCH
END
GO

/****** Object:  StoredProcedure [dbo].[uspUpdateSponsorer]    Script Date: 3/8/2020 6:51:02 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Anish.Bapat>
-- Create date: <11-02-2020>
-- Description:	<Update  Sponsorer Details>
-- =============================================
CREATE PROCEDURE [dbo].[uspUpdateSponsorer]
	@SponsorerId bigint,
	@SponsorerName nvarchar(200),
	@ThumbnailImage varbinary(MAX)=null,
	@LandingImage varbinary(MAX)=null,
	@IsActive bit = null,
	@CreatedBy bigint = null,
	@ModifiedBy bigint = null,
	@ReturnId bigint OUT
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRANSACTION 
	BEGIN TRY
       UPDATE Mas_Sponsorer SET 
	   SponsorerName=@SponsorerName,
	   ThumbnailImage=@ThumbnailImage,
	   LandingImage=@LandingImage,
	   IsActive=@IsActive,
	   CreatedBy=@CreatedBy,
	   ModifiedBy=@ModifiedBy,
	   ModifiedDate = GETDATE()
	   WHERE SponsorerId=@SponsorerId

	   SET @ReturnId =@@ROWCOUNT
	   COMMIT TRANSACTION ;
    END TRY
    BEGIN CATCH
		INSERT INTO Mas_ErrorLogs(ErrorProcedure,ErrorLine,ErrorMessage)
		VALUES(ERROR_PROCEDURE(),ERROR_LINE(),ERROR_MESSAGE())
		SET @ReturnId =0
		ROLLBACK TRANSACTION ;
    END CATCH
END
GO

/****** Object:  StoredProcedure [dbo].[uspInsertLinkSponsorerCoupon]    Script Date: 3/8/2020 6:52:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Anish.Bapat>
-- Create date: <11-02-2020>
-- Description:	<Retrive all Subject Information>
-- =============================================
CREATE PROCEDURE [dbo].[uspInsertLinkSponsorerCoupon]
	@FromCouponId bigint,
	@ToCouponId bigint,
	@SponsorerId bigint,
	@RowCount bigint OUT
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRANSACTION 
	BEGIN TRY
       INSERT INTO  Lnk_CouponDetails(CouponId,SponsorerId) 
	   SELECT mc.CouponId,@SponsorerId FROM Mas_Coupon mc WHERE mc.CouponId BETWEEN @FromCouponId and @ToCouponId
	   SET @RowCount =SCOPE_IDENTITY()
	   COMMIT TRANSACTION ;
    END TRY
    BEGIN CATCH
		INSERT INTO Mas_ErrorLogs(ErrorProcedure,ErrorLine,ErrorMessage)
		VALUES(ERROR_PROCEDURE(),ERROR_LINE(),ERROR_MESSAGE())
		SET @RowCount =0
		ROLLBACK TRANSACTION ;
    END CATCH
END
GO