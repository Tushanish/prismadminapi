﻿/****** Object:  Table [dbo].[Lnk_CouponDetails]    Script Date: 3/7/2020 9:55:16 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Lnk_CouponDetails](
	[CouponDetailsId] [bigint] IDENTITY(1,1) NOT NULL,
	[CouponId] [nvarchar](max) NOT NULL,
	[SponsorerId] [int] NOT NULL,
 CONSTRAINT [PK_Lnk_CouponDetails] PRIMARY KEY CLUSTERED 
(
	[CouponDetailsId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Mas_Coupon]    Script Date: 3/7/2020 9:55:58 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Mas_Coupon](
	[CouponId] [bigint] IDENTITY(1,1) NOT NULL,
	[CouponCode] [nvarchar](max) NOT NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_Mas_Coupon_IsActive]  DEFAULT ((0)),
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL CONSTRAINT [DF_Mas_Coupon_CreatedDate]  DEFAULT (getdate()),
	[ModifiedBy] [nvarchar](50) NULL,
	[ModifiedDate] [datetime] NULL CONSTRAINT [DF_Mas_Coupon_ModifiedDate]  DEFAULT (getdate()),
 CONSTRAINT [PK_Mas_Coupon] PRIMARY KEY CLUSTERED 
(
	[CouponId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

/****** Object:  StoredProcedure [dbo].[uspActiveDeactivateCoupon]    Script Date: 3/8/2020 7:07:58 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Anish.Bapat>
-- Create date: <07-03-2020>
-- Description:	<Acitvate Deactivate Sponsorer>
-- =============================================
CREATE PROCEDURE [dbo].[uspActiveDeactivateCoupon]
	@CouponId bigint,
	@IsActive bit,
	@RowCount int OUT
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRANSACTION 
	BEGIN TRY
       UPDATE Mas_Coupon SET 
	   IsActive=@IsActive,
	   ModifiedDate = GETDATE()
	   WHERE CouponId=@CouponId
	   SET @RowCount= @@RowCount
	   COMMIT TRANSACTION ;
    END TRY
    BEGIN CATCH
		INSERT INTO Mas_ErrorLogs(ErrorProcedure,ErrorLine,ErrorMessage)
		VALUES(ERROR_PROCEDURE(),ERROR_LINE(),ERROR_MESSAGE())
		SET @RowCount=0
		ROLLBACK TRANSACTION ;
    END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[uspGetAllLnkCouponDetails]    Script Date: 3/8/2020 7:08:27 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Anish.Bapat>
-- Create date: <07-03-2020>
-- Description:	<Retrive all Subject Information>
-- =============================================
CREATE PROCEDURE [dbo].[uspGetAllLnkCouponDetails] 
	@CouponId bigint=null,
	@SponsorerId bigint=null

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
   
	SELECT LCD.CouponDetailsId,LCD.CouponId,LCD.SponsorerId FROM Lnk_CouponDetails LCD
	WHERE (ISNULL(@SponsorerId,0)=0 OR LCD.SponsorerId=@SponsorerId) AND (ISNULL(@CouponId,0)=0 OR LCD.CouponId=@CouponId)
END
GO

/****** Object:  StoredProcedure [dbo].[uspGetAllCoupons]    Script Date: 3/8/2020 7:08:51 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Anish.Bapat>
-- Create date: <11-02-2020>
-- Description:	<Retrive all Subject Information>
-- =============================================
CREATE PROCEDURE [dbo].[uspGetAllCoupons] 
	@CouponId bigint=null,
	@IsActive bit=null

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
   
	SELECT MC.CouponId,MC.CouponCode,MC.IsActive,MC.CreatedBy,MC.CreatedDate,MC.ModifiedBy,MC.ModifiedDate FROM Mas_Coupon MC
	WHERE (ISNULL(@CouponId,0)=0 OR MC.CouponId=@CouponId) AND (ISNULL(@IsActive,0)=0 OR MC.IsActive=@IsActive)
END
GO