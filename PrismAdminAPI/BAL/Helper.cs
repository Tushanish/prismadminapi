﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PrismAdminAPI.BAL
{
    public class Helper
    {
        public static byte[] GetByteArray(HttpPostedFileBase _subjectThumbnail)
        {
            byte[] uploadedFile = null;
            if (_subjectThumbnail != null)
            {
                uploadedFile = new byte[_subjectThumbnail.InputStream.Length];
                _subjectThumbnail.InputStream.Read(uploadedFile, 0, uploadedFile.Length);
            }
            return uploadedFile;
        }
    }
}