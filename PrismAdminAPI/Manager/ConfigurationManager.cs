﻿using PrismAdminAPI.Models;
using PrismAdminAPI.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PrismAdminAPI.Manager
{
    public class ConfigurationManager : IConfigurationManager
    {
        private IConfigurationRepository configurationRepository;

        public ConfigurationManager(IConfigurationRepository _configurationRepository)
        {
            configurationRepository = _configurationRepository;
        }

        public IEnumerable<StandardMedium> GetStandardMediamConfiguration(long? _standardMediumId = null)
        {
            return configurationRepository.GetStandardMediamConfiguration(_standardMediumId);
        }

        public long InsertStandardMedium(StandardMedium _standardMedium)
        {
            return configurationRepository.InsertStandardMedium(_standardMedium);
        }

        public IEnumerable<StandardMedium> GetConfiguredStandardMediamConfiguration(long? _standardId = null)
        {
            return configurationRepository.GetConfiguredStandardMediamConfiguration(_standardId);
        }
    }
}