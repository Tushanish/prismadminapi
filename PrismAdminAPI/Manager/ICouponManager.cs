﻿using System.Collections.Generic;
using PrismAdminAPI.Models;

namespace PrismAdminAPI.Manager
{
    public interface ICouponManager
    {
        IEnumerable<Coupon> GetAllCoupons(long? couponId = null, bool? isActive = null);
        bool SaveBulkCoupons<T>(int batchSize, string tableName, List<T> _list);
        bool ActivateDeactivateCoupon(long? couponId, bool IsActive);
        IEnumerable<LnkCouponDetails> GetAllLnkCouponDetails(long? couponId = null, long? sponsorerId = null);
    }
}