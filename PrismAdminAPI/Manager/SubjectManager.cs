﻿using PrismAdminAPI.Models;
using PrismAdminAPI.Repository;
using PrismAdminAPI.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PrismAdminAPI.Manager
{
    public class SubjectManager : ISubjectManager
    {
        private ISubjectRepository subjectRepository;
        public SubjectManager()
        {
            subjectRepository = new SubjectRepository();
        }
        public IEnumerable<SubjectViewModel> GetSubjectList(long? subjectId = null, bool? isActive = null)
        {
            return subjectRepository.GetSubjectList(subjectId,isActive);
        }

        public long InsertSubject(Subject _subject)
        {
            return subjectRepository.InsertSubject(_subject);
        }

        public int UpdateSubject(Subject _subject)
        {
            return subjectRepository.UpdateSubject(_subject);
        }

        public int ActivateDeactivateSubject(long _subjectId, bool _isActive)
        {
            return subjectRepository.ActivateDeactivateSubject(_subjectId, _isActive);
        }
    }
}