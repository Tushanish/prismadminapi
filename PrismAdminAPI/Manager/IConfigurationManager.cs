﻿using PrismAdminAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrismAdminAPI.Manager
{
    public interface IConfigurationManager
    {
        IEnumerable<StandardMedium> GetStandardMediamConfiguration(long? _standardMediumId = null);

        long InsertStandardMedium(StandardMedium _standardMedium);

        IEnumerable<StandardMedium> GetConfiguredStandardMediamConfiguration(long? _standardId = null);
    }
}
