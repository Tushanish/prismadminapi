﻿using PrismAdminAPI.Models;
using PrismAdminAPI.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PrismAdminAPI.Manager
{
    public class MediumManager : IMediumManager
    {
        private IMediumRepository _iMediumRepository = null;
        public MediumManager()
        {
            _iMediumRepository = new MediumRepository();
        }
        public IEnumerable<Medium> GetMediumList(long? _mediumId = null, bool? _isActive = null)
        {
            return _iMediumRepository.GetMediumList(_mediumId, _isActive);
        }

        public long InsertMedium(Medium _medium)
        {
            return _iMediumRepository.InsertMedium(_medium);
        }

        public int UpdateMedium(Medium _medium)
        {
            return _iMediumRepository.UpdateMedium(_medium);
        }

        public int ActivateDeactivateMedium(long _mediumId, bool _isActive)
        {
            return _iMediumRepository.ActivateDeactivateMedium(_mediumId, _isActive);
        }
    }
}