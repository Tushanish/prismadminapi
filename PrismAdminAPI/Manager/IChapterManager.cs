﻿using PrismAdminAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrismAdminAPI.Manager
{
    public interface IChapterManager
    {
        IEnumerable<Chapter> GetChapterList(long? chapterId = null, bool? isActive = null);

        long InsertChapter(Chapter _chapter);

        int UpdateChapter(Chapter _chapter);

        int ActivateDeactivateChapter(long _chapterId, bool _isActive);
    }
}
