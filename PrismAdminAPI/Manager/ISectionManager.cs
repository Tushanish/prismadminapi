﻿using PrismAdminAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrismAdminAPI.Manager
{
    public interface ISectionManager
    {
        IEnumerable<Section> GetSectionList(long? _sectionId = null, bool? _isActive = null);

        long InsertSection(Section _section);

        int UpdateSection(Section _section);

        int ActivateDeactivateSection(long _sectionId, bool _isActive);

        int ActivateDeactivateStandardSection(long _sectionId, bool _isStandardSection);

        int ActivateDeactivateChapterSection(long _sectionId, bool _isChapterSection);
    }
}
