﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PrismAdminAPI.Models;
using PrismAdminAPI.Repository;

namespace PrismAdminAPI.Manager
{
    public class SponsorerManager : ISponsorerManager
    {
        private readonly ISponsorerRepository _sponsorerRepository;

        public SponsorerManager(ISponsorerRepository sponsorerRepository)
        {
            _sponsorerRepository = sponsorerRepository;
        }

        public bool AddSponsorer(Sponsorer sponsorer)
        {
            return _sponsorerRepository.AddSponsorer(sponsorer);
        }

        public IEnumerable<Sponsorer> GetAll(long? sponsorerId = null, bool? isActive = null)
        {
            return _sponsorerRepository.GetAll(sponsorerId, isActive);
        }

        public bool UpdateSponsorer(Sponsorer sponsorer)
        {
            return _sponsorerRepository.UpdateSponsorer(sponsorer);
        }

        public bool AssignSponsorerToCoupon(long? fromCouponId, long? toCouponId, long? sponsorerId)
        {
            return _sponsorerRepository.AssignSponsorerToCoupon(fromCouponId, toCouponId, sponsorerId);
        }

        public bool AcitvateDeactivateSponsorer(long sponsererId, bool IsActive)
        {
            return _sponsorerRepository.ActivateDeactivateSponsorer(sponsererId, IsActive);
        }
    }
}