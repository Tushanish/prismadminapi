﻿using PrismAdminAPI.Models;
using PrismAdminAPI.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PrismAdminAPI.Manager
{
    public class ChapterManager : IChapterManager
    {
        IChapterRepository chapterRepository = null;
        public ChapterManager() {
            chapterRepository = new ChapterRepository();
        }

        public IEnumerable<Chapter> GetChapterList(long? chapterId = null, bool? isActive = null)
        {
            return chapterRepository.GetChapterList(chapterId, isActive);
        }

        public long InsertChapter(Chapter _chapter)
        {
            return chapterRepository.InsertChapter(_chapter);
        }

        public int UpdateChapter(Chapter _chapter)
        {
            return chapterRepository.UpdateChapter(_chapter);
        }

        public int ActivateDeactivateChapter(long _chapterId, bool _isActive)
        {
            return chapterRepository.ActivateDeactivateChapter(_chapterId, _isActive);
        }
    }
}