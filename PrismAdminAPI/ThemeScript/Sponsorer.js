﻿var _alertDanger = "alert-danger"
var _alertSuccess = "alert-success";
var isSubjectEdit = false;

function ActiveDeactive(eval) {
    var _sponsorer = {};
    _sponsorer.SponsorerId = $(eval).attr("data-serialid");
    _sponsorer.IsActive = $(eval).prop("checked");
    debugger;
    $.ajax({
        type: "POST",
        url: _urlActiveDeactiveSubject,
        data: '{_sponsorer: ' + JSON.stringify(_sponsorer) + '}',
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function () {
            SuccessMessage($("#divAlertMessage"), "Data updated successfully!");
        },
        error: function () {
            debugger;
            ErrorMessage($("#divAlertMessage"), "Error while updating the data!");
        }
    });
    return false;
}

$(document).ready(function () {
    //bsCustomFileInput.init();
});

function SuccessMessage(eval, textMessage) {
    $(eval).removeClass("d-none");
    $(eval).removeClass(_alertDanger);
    $(eval).removeClass(_alertSuccess);
    $(eval).find(".alert").addClass(_alertSuccess);
    $(eval).fadeIn(200).delay(3000).fadeOut(200)
    $(eval).find(".alert").find(".textMessage").text(textMessage);
}
function ErrorMessage(eval, textMessage) {
    $(eval).removeClass("d-none");
    $(eval).removeClass(_alertDanger);
    $(eval).removeClass(_alertSuccess);
    $(eval).find(".alert").addClass(_alertDanger);
    $(eval).fadeIn(200).delay(3000).fadeOut(200)
    $(eval).find(".alert").find(".textMessage").text(textMessage);
}

function GetCheckBoxHTML(sponsererId) {
    var checked = "";
    if (isSponsererEdit && _mediumIdList.indexOf(mediumId) !== -1) {
        checked = "checked";
    }
    else {
        checked = "";
    }
    var standardMedium = "chkStandardMedium-" + standardId + "-" + mediumId;
    var mediumcheckbox = '<div class="custom-control custom-checkbox">';
    mediumcheckbox += '<input class="custom-control-input" data-standardId="' + standardId + '" data-mediumId="' + mediumId + '" onclick="GetMediumLists()" type="checkbox" id="' + standardMedium + '"' + checked + '>';
    mediumcheckbox += '<label for="' + standardMedium + '" class="custom-control-label">' + mediumName + '</label>';
    mediumcheckbox += '</div>';
    return mediumcheckbox
}