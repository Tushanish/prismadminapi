﻿var _alertDanger = "alert-danger"
var _alertSuccess = "alert-success";
var isSubjectEdit = false;
var _mediumIdList = [];
function ActiveDeactive(eval) {
    var _subject = {};
    _subject.SubjectId = $(eval).attr("data-serialid");
    _subject.IsActive = $(eval).prop("checked");
    $.ajax({
        type: "POST",
        url: _urlActiveDeactiveSubject,
        data: '{_subject: ' + JSON.stringify(_subject) + '}',
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function () {
            SuccessMessage($("#divAlertMessage"), "Data updated successfully!");
        },
        error: function () {
            ErrorMessage($("#divAlertMessage"), "Error while updating the data!");
        }
    });
    return false;
}

$(document).ready(function () {
    bsCustomFileInput.init();

    $(".ddlStandard").change(function () {
        $(".div-configured-mediums").empty();
        if (parseInt($(this).val()) > 0) {
            GetMediumsBySubjectId($(this).val());
        }
        else {
            $(".div-configured-mediums").append("Configured Medium List (Please select standard)");
        }
    });

    // On edit get selected mediums 
    isSubjectEdit = $("#SubjectId").length > 0 ? true : false;
    if (isSubjectEdit) {
        $(".div-configured-mediums").empty();
        GetMediumsBySubjectId($("#StandardId").val());
        _mediumIdList = $("#MediumIdList").val().split(',').map(function (item) {
            return parseInt(item, 10);
        });;
    }
});


function GetMediumsBySubjectId(standardId) {
    
    $.ajax({
        type: 'GET',
        url: _urlGetMediumsBySubjectId,
        dataType: 'json',
        data: { _standardId: standardId },
        success: function (mediums) {
            if (mediums.length > 0) {
                $.each(mediums, function (i, medium) {
                    $(".div-configured-mediums").append(GetCheckBoxHTML(medium.StandardId,medium.MediumId,medium.MediumName));
                });
            }
            else {
                ErrorMessage($("#divAlertMessage"), "Please configure the mediums for standards!");
            }
        },
        error: function (ex) {
            ErrorMessage($("#divAlertMessage"), "Error while updating the data!");
        }
    });
    return false;
}

function SuccessMessage(eval, textMessage) {
    $(eval).removeClass("d-none");
    $(eval).removeClass(_alertDanger);
    $(eval).removeClass(_alertSuccess);
    $(eval).find(".alert").addClass(_alertSuccess);
    $(eval).fadeIn(200).delay(3000).fadeOut(200)
    $(eval).find(".alert").find(".textMessage").text(textMessage);
}
function ErrorMessage(eval, textMessage) {
    $(eval).removeClass("d-none");
    $(eval).removeClass(_alertDanger);
    $(eval).removeClass(_alertSuccess);
    $(eval).find(".alert").addClass(_alertDanger);
    $(eval).fadeIn(200).delay(3000).fadeOut(200)
    $(eval).find(".alert").find(".textMessage").text(textMessage);
}

function GetCheckBoxHTML(standardId, mediumId, mediumName) {
    var checked = "";
    if (isSubjectEdit && _mediumIdList.indexOf(mediumId) !== -1) {
        checked = "checked";
    }
    else {
        checked = "";
    }
    var standardMedium = "chkStandardMedium-" + standardId + "-" + mediumId;
    var mediumcheckbox = '<div class="custom-control custom-checkbox">';
    mediumcheckbox += '<input class="custom-control-input" data-standardId="' + standardId + '" data-mediumId="' + mediumId + '" onclick="GetMediumLists()" type="checkbox" id="' + standardMedium + '"' + checked+'>';
    mediumcheckbox += '<label for="' + standardMedium + '" class="custom-control-label">' + mediumName+'</label>';
    mediumcheckbox += '</div>';
    return mediumcheckbox
}

function GetMediumLists() {
    $("#MediumIdList").val("");
    var selected = new Array();

    //Reference the CheckBoxes and insert the checked CheckBox value in Array.
    $(".div-configured-mediums").find("input[type=checkbox]:checked").each(function () {
        selected.push($(this).attr("data-mediumId"));
    });
    if (selected.length > 0) {
        $("#MediumIdList").val(selected.join(","))
    }
}
