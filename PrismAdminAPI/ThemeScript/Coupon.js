﻿var _alertDanger = "alert-danger"
var _alertSuccess = "alert-success";
var isSubjectEdit = false;

function ActiveDeactive(eval) {
    var _coupon = {};
    _coupon.CouponId= $(eval).attr("data-serialid");
    _coupon.IsActive = $(eval).prop("checked");
    debugger;
    $.ajax({
        type: "POST",
        url: _urlActiveDeactivateCoupon,
        data: '{_coupon: ' + JSON.stringify(_coupon) + '}',
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function () {
            SuccessMessage($("#divAlertMessage"), "Data updated successfully!");
        },
        error: function () {
            ErrorMessage($("#divAlertMessage"), "Error while updating the data!");
        }
    });
    return false;
}

$(document).ready(function () {
    //bsCustomFileInput.init();
});

function SuccessMessage(eval, textMessage) {
    $(eval).removeClass("d-none");
    $(eval).removeClass(_alertDanger);
    $(eval).removeClass(_alertSuccess);
    $(eval).find(".alert").addClass(_alertSuccess);
    $(eval).fadeIn(200).delay(3000).fadeOut(200)
    $(eval).find(".alert").find(".textMessage").text(textMessage);
}
function ErrorMessage(eval, textMessage) {
    $(eval).removeClass("d-none");
    $(eval).removeClass(_alertDanger);
    $(eval).removeClass(_alertSuccess);
    $(eval).find(".alert").addClass(_alertDanger);
    $(eval).fadeIn(200).delay(3000).fadeOut(200)
    $(eval).find(".alert").find(".textMessage").text(textMessage);
}

