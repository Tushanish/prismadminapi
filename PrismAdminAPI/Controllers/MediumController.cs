﻿using PrismAdminAPI.Manager;
using PrismAdminAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PrismAdminAPI.Controllers
{
    public class MediumController : Controller
    {
        private readonly IMediumManager mediumManager;
        public MediumController(IMediumManager _mediumManager)
        {
            mediumManager = _mediumManager;
        }

        /// <summary>
        /// get all medium list 
        /// </summary>
        /// <returns></returns>
        [Route("mediumlist")]
        [Route("Medium/ListMedium")]
        public ActionResult ListMedium()
        {
            IEnumerable<Medium> _mediumList = mediumManager.GetMediumList();
            return View(_mediumList);
        }

        /// <summary>
        /// Add Medium
        /// </summary>
        /// <returns></returns>
        [Route("addmedium")]
        [Route("Medium/AddMedium")]
        public ActionResult AddMedium()
        {
            return View();
        }

        /// <summary>
        /// Insert Medium 
        /// </summary>
        /// <param name="_medium"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult InsertMedium(Medium _medium)
        {
            long _insertResult = mediumManager.InsertMedium(_medium);
            return RedirectToAction("ListMedium");
        }

        /// <summary>
        /// Update Standard
        /// </summary>
        /// <returns></returns>
        [Route("editmedium/{mediumId}")]
        [Route("Medium/EditMedium/{mediumId}")]
        public ActionResult EditMedium(int mediumId)
        {
            Medium _medium = mediumManager.GetMediumList(mediumId).FirstOrDefault();
            if (_medium == null)
            {
                return View("ListMedium");
            }
            return View(_medium);
        }

        /// <summary>
        /// Update medium 
        /// </summary>
        /// <param name="_medium"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult UpdateMedium(Medium _medium)
        {
            long _updateResult = mediumManager.UpdateMedium(_medium);
            return RedirectToAction("ListMedium");
        }

        /// <summary>
        /// Activate or Deactivate the medium
        /// </summary>
        /// <param name="_medium"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult ActiveDeactiveMedium(Medium _medium)
        {
            long _activateDeactivateResult = mediumManager.ActivateDeactivateMedium(_medium.MediumId, _medium.IsActive);
            return Json(_activateDeactivateResult);
        }
    }
}