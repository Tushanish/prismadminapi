﻿using PrismAdminAPI.BAL;
using PrismAdminAPI.Manager;
using PrismAdminAPI.Models;
using PrismAdminAPI.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PrismAdminAPI.Controllers
{
    public class CouponController : Controller
    {

        private readonly ICouponManager _couponManager;
        private readonly ISponsorerManager _sponsoreManager;

        public CouponController(ICouponManager couponManager, ISponsorerManager sponsorerManager)
        {
            _couponManager = couponManager;
            _sponsoreManager = sponsorerManager;
        }


        [Route("addCoupon")]
        [Route("Coupon/AddCoupon")]
        public ActionResult AddCoupon()
        {
            return View();
        }

        /// <summary>
        /// create coupon
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("addCoupon")]
        [Route("Coupon/AddCoupon")]
        public ActionResult AddCoupon(int noOfCoupons = 0)
        {
            var couponList = CreateCouponObjects(noOfCoupons);
            var returnVal = _couponManager.SaveBulkCoupons(noOfCoupons, "dbo.Mas_Coupon", couponList);
            if (returnVal)
            {
                TempData["Success"] = 1;
            }
            else
            {
                TempData["Success"] = 0;
            }
            return RedirectToAction("ListCoupon");
        }


        /// <summary>
        /// get all coupon details list 
        /// </summary>
        /// <returns></returns>
        [Route("couponlist")]
        [Route("Coupon/ListCouponSponsorerDetails")]
        public ActionResult ListCoupon()
        {
            var couponSponsorerList = GetCouponSponsorerDetails();
            return View(couponSponsorerList);
        }

        /// <summary>
        /// Activate or Deactivate the coupon
        /// </summary>
        /// <param name="_coupon">Coupon instance</param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult ActiveDeactivateCoupon(Coupon _coupon)
        {
            bool _activateDeactivateResult = _couponManager.ActivateDeactivateCoupon(_coupon.CouponId,_coupon.IsActive);
            return Json(_activateDeactivateResult);
        }

        [NonAction]
        public List<CouponSponsorerViewModel> GetCouponSponsorerDetails()
        {
            List<CouponSponsorerViewModel> couponSponsorerViewModels = new List<CouponSponsorerViewModel>();
            var lnkCoupons = _couponManager.GetAllLnkCouponDetails();
            var couponDetails = _couponManager.GetAllCoupons();
            var sponsorerDetails = _sponsoreManager.GetAll();
            foreach (var lnkCoupon in lnkCoupons)
            {
                var sponsorer = sponsorerDetails.FirstOrDefault(x => x.SponsorerId == lnkCoupon.SponsorerId);
                var coupon = couponDetails.FirstOrDefault(x => x.CouponId == lnkCoupon.CouponId);

                couponSponsorerViewModels.Add(new CouponSponsorerViewModel
                {
                    CouponCode = coupon.CouponCode,
                    CouponId = coupon.CouponId,
                    CreatedDate = coupon.CreatedDate,
                    IsActive = coupon.IsActive,
                    CreatedBy = coupon.CreatedBy,
                    ModifiedBy = coupon.ModifiedBy,
                    ModifiedDate = coupon.ModifiedDate,
                    SponsorerId = sponsorer.SponsorerId,
                    SponsorerName = sponsorer.SponsorerName
                });
            }
            foreach (var coupon in couponDetails)
            {
                if(!lnkCoupons.Any(x=>x.CouponId == coupon.CouponId))
                {
                    couponSponsorerViewModels.Add(new CouponSponsorerViewModel
                    {
                        CouponCode = coupon.CouponCode,
                        CouponId = coupon.CouponId,
                        CreatedDate = coupon.CreatedDate,
                        IsActive = coupon.IsActive,
                        CreatedBy = coupon.CreatedBy,
                        ModifiedBy = coupon.ModifiedBy,
                        ModifiedDate = coupon.ModifiedDate,
                    });
                }
            }
            return couponSponsorerViewModels;
        }

        [NonAction]
        public List<Coupon> CreateCouponObjects(int noOfCoupons)
        {
            var couponList = new List<Coupon>();
            for (int i = 0; i < noOfCoupons; i++)
            {
                couponList.Add(new Coupon { CouponCode = CouponGenerator.RandomString() });
            }
            return couponList;
        }
    }
}