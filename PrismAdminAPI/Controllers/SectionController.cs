﻿using PrismAdminAPI.Manager;
using PrismAdminAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PrismAdminAPI.Controllers
{
    public class SectionController : Controller
    {
        public ISectionManager sectionManager;
        public SectionController(ISectionManager _sectionManager)
        {
            sectionManager = _sectionManager;
        }
        /// <summary>
        /// get all section list 
        /// </summary>
        /// <returns></returns>
        [Route("sectionlist")]
        [Route("Section/ListSection")]
        public ActionResult ListSection()
        {
            IEnumerable<Section> _sectionList = sectionManager.GetSectionList();
            return View(_sectionList);
        }

        /// <summary>
        /// Add Section
        /// </summary>
        /// <returns></returns>
        [Route("addsection")]
        [Route("Section/AddSection")]
        public ActionResult AddSection()
        {
            return View();
        }

        /// <summary>
        /// Insert section 
        /// </summary>
        /// <param name="_section"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult InsertSection(Section _section)
        {
            long _insertResult = sectionManager.InsertSection(_section);
            return RedirectToAction("ListSection");
        }

        /// <summary>
        /// Update Section
        /// </summary>
        /// <returns></returns>
        [Route("editsection/{sectionId}")]
        [Route("Section/EditSection/{sectionId}")]
        public ActionResult EditSection(int sectionId)
        {
            Section _section = sectionManager.GetSectionList(sectionId).FirstOrDefault();
            if (_section == null)
            {
                return View("ListSection");
            }
            return View(_section);
        }

        /// <summary>
        /// Update section 
        /// </summary>
        /// <param name="_section"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult UpdateSection(Section _section)
        {
            long _updateResult = sectionManager.UpdateSection(_section);
            return RedirectToAction("ListSection");
        }

        /// <summary>
        /// Activate or Deactivate the section
        /// </summary>
        /// <param name="_section"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult ActiveDeactiveSection(Section _section)
        {
            long _activateDeactivateResult = sectionManager.ActivateDeactivateSection(_section.SectionId, _section.IsActive);
            return Json(_activateDeactivateResult);
        }

        /// <summary>
        /// Activate or Deactivate the standard section
        /// </summary>
        /// <param name="_section"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult ActiveDeactiveStandardSection(Section _section)
        {
            long _activateDeactivateResult = sectionManager.ActivateDeactivateStandardSection(_section.SectionId, _section.IsStandardSection);
            return Json(_activateDeactivateResult);
        }

        /// <summary>
        /// Activate or Deactivate the chapter section
        /// </summary>
        /// <param name="_section"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult ActiveDeactiveChapterSection(Section _section)
        {
            long _activateDeactivateResult = sectionManager.ActivateDeactivateChapterSection(_section.SectionId, _section.IsChapterSection);
            return Json(_activateDeactivateResult);
        }
    }
}