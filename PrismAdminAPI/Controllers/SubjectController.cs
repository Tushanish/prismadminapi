﻿using PrismAdminAPI.Manager;
using PrismAdminAPI.Models;
using PrismAdminAPI.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PrismAdminAPI.Controllers
{
    public class SubjectController : Controller
    {
        private readonly IStandardManager standardManager;
        private readonly IMediumManager mediumManager;
        private readonly IConfigurationManager configurationManager;
        private readonly ISubjectManager subjectManager;
        public SubjectController(IStandardManager _standardManager, 
            IMediumManager _mediumManager, 
            IConfigurationManager _configurationManager,
            ISubjectManager _subjectManager)
        {
            standardManager = _standardManager;
            mediumManager = _mediumManager;
            configurationManager = _configurationManager;
            subjectManager = _subjectManager;
        }

        [Route("subjectadd")]
        [Route("Subject/AddSubject")]
        public ActionResult AddSubject()
        {
            Subject _subject = new Subject();
            _subject = GetSubjectObjectDetails();
            return View(_subject);
        }

        [HttpPost]
        public ActionResult InsertSubject(Subject _subject)
        {
            long addResult = subjectManager.InsertSubject(_subject);
            return RedirectToAction("Subject/AddSubject");
        }


        [HttpGet]
        public JsonResult GetMediumsBySubjectId(long _standardId)
        {
            return Json(configurationManager.GetConfiguredStandardMediamConfiguration(_standardId),JsonRequestBehavior.AllowGet);
        }

        [NonAction]
        public Subject GetSubjectObjectDetails(IEnumerable<SubjectViewModel> _subjectViewModelList=null)
        {
            Subject _subject = new Subject();
            if (_subjectViewModelList!=null)
            {
                SubjectViewModel subjectViewModel = _subjectViewModelList.FirstOrDefault();
                _subject.SubjectId = subjectViewModel.SubjectId;
                _subject.SubjectName = subjectViewModel.SubjectName;
                _subject.Description = subjectViewModel.Description;
                _subject.StandardId = subjectViewModel.StandardId;
                _subject.MediumIdList = string.Join(",", _subjectViewModelList.Where(x=>x.IsSubjectMediumActive==true).Select(x => x.MediumId));
                _subject.SubjectViewModel = subjectViewModel;
            }
            _subject.StandardList = standardManager.GetStandardList(null, true).Select(x => new SelectListItem { Text = String.Format("{0} ({1})", x.StandardName, x.Description), Value = x.StandardId.ToString() });
            return _subject;
        }

        [Route("subjectlist")]
        [Route("Subject/ListSubject")]
        public ActionResult ListSubject()
        {
            var subjectList = subjectManager.GetSubjectList();
            return View(subjectList);
        }

        [HttpPost]
        public JsonResult ActiveDeactiveSubject(Subject _subject)
        {
            long _activateDeactivateResult = subjectManager.ActivateDeactivateSubject(_subject.SubjectId, _subject.IsActive);
            return Json(_activateDeactivateResult);
        }

        [Route("editsubject/{subjectId}")]
        [Route("Subject/EditSubject/{subjectId}")]
        public ActionResult EditSubject(long subjectId)
        {
            IEnumerable<SubjectViewModel> _subjectViewModelList = subjectManager.GetSubjectList(subjectId);
            
            Subject _subject = new Subject();
            if (_subjectViewModelList.Count() == 0)
            {
                return View("ListStandard");
            }
            else
            {
                _subject = GetSubjectObjectDetails(_subjectViewModelList);
            }
            return View(_subject);
        }

        [HttpPost]
        public ActionResult UpdateSubject(Subject _subject)
        {
            long updateResult = subjectManager.UpdateSubject(_subject);
            return RedirectToAction("ListSubject");
        }
    }
}