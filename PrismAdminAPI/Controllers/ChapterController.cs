﻿using PrismAdminAPI.Manager;
using PrismAdminAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PrismAdminAPI.Controllers
{
    public class ChapterController : Controller
    {
        private readonly IChapterManager chapterManager;
        private readonly IStandardManager standardManager;
        public ChapterController(IChapterManager _chapterManager, IStandardManager _standardManager)
        {
            chapterManager = _chapterManager;
            standardManager = _standardManager;
        }

        /// <summary>
        /// get all standard list 
        /// </summary>
        /// <returns></returns>
        [Route("chapterlist")]
        [Route("Chapter/ListChapter")]
        public ActionResult ListChapter()
        {
            IEnumerable<Chapter> _standardList = chapterManager.GetChapterList();
            return View(_standardList);
        }

        /// <summary>
        /// Add Standard
        /// </summary>
        /// <returns></returns>
        [Route("addchapter")]
        [Route("Chapter/AddChapter")]
        public ActionResult AddChapter()
        {
            Chapter _chapter = new Chapter();
            _chapter = GetChapterObjectDetails();
            return View(_chapter);
        }

        [NonAction]
        public Chapter GetChapterObjectDetails()
        {
            Chapter _chapter = new Chapter();
            _chapter.StandardList = standardManager.GetStandardList(null, true).Select(x => new SelectListItem { Text = String.Format("{0} ({1})", x.StandardName, x.Description), Value = x.StandardId.ToString() });
            return _chapter;
        }
    }
}