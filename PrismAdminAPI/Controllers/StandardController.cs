﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PrismAdminAPI.Manager;
using PrismAdminAPI.Models;

namespace PrismAdminAPI.Controllers
{
    public class StandardController : Controller
    {
        private readonly IStandardManager standardManager;
        public StandardController(IStandardManager _standardManager)
        {
            standardManager = _standardManager;
        }
        
        /// <summary>
        /// get all standard list 
        /// </summary>
        /// <returns></returns>
        [Route("standardlist")]
        [Route("Standard/ListStandard")]
        public ActionResult ListStandard()
        {
            IEnumerable<Standard> _standardList = standardManager.GetStandardList();
            return View(_standardList);
        }

        /// <summary>
        /// Add Standard
        /// </summary>
        /// <returns></returns>
        [Route("addstandard")]
        [Route("Standard/AddStandard")]
        public ActionResult AddStandard()
        {
            return View();
        }

        /// <summary>
        /// Insert standard 
        /// </summary>
        /// <param name="_standard"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult InsertStandard(Standard _standard)
        {
            long _insertResult = standardManager.InsertStandard(_standard);
            return RedirectToAction("ListStandard");
        }

        /// <summary>
        /// Update Standard
        /// </summary>
        /// <returns></returns>
        [Route("editstandard/{standardId}")]
        [Route("Standard/EditStandard/{standardId}")]
        public ActionResult EditStandard(int standardId)
        {
            Standard _standard = standardManager.GetStandardList(standardId).FirstOrDefault();
            if(_standard==null)
            {
                return View("ListStandard");
            }
            return View(_standard);
        }

        /// <summary>
        /// Update standard 
        /// </summary>
        /// <param name="_standard"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult UpdateStandard(Standard _standard)
        {
            long _updateResult = standardManager.UpdateStandard(_standard);
            return RedirectToAction("ListStandard");
        }

        /// <summary>
        /// Activate or Deactivate the standard
        /// </summary>
        /// <param name="_standard"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult ActiveDeactiveStandard(Standard _standard)
        {
            long _activateDeactivateResult = standardManager.ActivateDeactivateStandard(_standard.StandardId,_standard.IsActive);
            return Json(_activateDeactivateResult);
        }
    }
}