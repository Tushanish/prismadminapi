﻿using PrismAdminAPI.Manager;
using PrismAdminAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PrismAdminAPI.Controllers
{
    public class ConfigurationController : Controller
    {
        private IConfigurationManager configurationManager;
        public ConfigurationController(IConfigurationManager _configurationManager)
        {
            configurationManager = _configurationManager;
        }

        // GET: Configuration
        [Route("standardmedium")]
        [Route("Configuration/StandardMedium")]
        public ActionResult StandardMedium()
        {
            var _standardMedium = configurationManager.GetStandardMediamConfiguration();
            return View(_standardMedium);
        }
        /// <summary>
        /// Activate or Deactivate the standard/Medium
        /// </summary>
        /// <param name="_standard"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult ActiveDeactiveStandardMedium(StandardMedium _standardMedium)
        {
            long _activateDeactivateResult = configurationManager.InsertStandardMedium(_standardMedium);
            return Json(_activateDeactivateResult);
        }

    }
}