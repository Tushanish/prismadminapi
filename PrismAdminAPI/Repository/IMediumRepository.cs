﻿using PrismAdminAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrismAdminAPI.Repository
{
    public interface IMediumRepository
    {
        IEnumerable<Medium> GetMediumList(long? _mediumId = null, bool? _isActive = null);

        long InsertMedium(Medium _medium);

        int UpdateMedium(Medium _medium);

        int ActivateDeactivateMedium(long _mediumId, bool _isActive);
    }
}
