﻿using Dapper;
using PrismAdminAPI.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace PrismAdminAPI.Repository
{
    public class MediumRepository : IMediumRepository
    {
        public IEnumerable<Medium> GetMediumList(long? _mediumId = null, bool? _isActive = null)
        {
            IList<Medium> _mediumList = new List<Medium>();
            try
            {
                using (SqlConnection _sqlConnection = new SqlConnection(WebConfig.ConnectionString))
                {
                    _sqlConnection.Open();
                    DynamicParameters _mediumParameters = new DynamicParameters();
                    _mediumParameters.Add("@MediumId", _mediumId);
                    _mediumParameters.Add("@IsActive", _isActive);

                    _mediumList = _sqlConnection.Query<Medium>("uspGetAllMediums", _mediumParameters, commandType: CommandType.StoredProcedure).ToList();
                    _sqlConnection.Close();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return _mediumList;
        }

        public long InsertMedium(Medium _medium)
        {
            long _addResult = 0;
            try
            {
                using (SqlConnection _sqlConnection = new SqlConnection(WebConfig.ConnectionString))
                {
                    _sqlConnection.Open();
                    DynamicParameters _mediumParameters = new DynamicParameters();
                    _mediumParameters.Add("@MediumName", _medium.MediumName);
                    _mediumParameters.Add("@Description", _medium.Description);
                    _mediumParameters.Add("@MediumId", _medium.MediumId, DbType.Int64, ParameterDirection.Output);

                    var result = _sqlConnection.Execute("uspInsertMedium", _mediumParameters, commandType: CommandType.StoredProcedure);
                    _addResult = _mediumParameters.Get<long>("MediumId");
                    _sqlConnection.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }
            return _addResult;
        }

        public int UpdateMedium(Medium _medium)
        {
            int _updateResult = 0;
            try
            {
                using (SqlConnection _sqlConnection = new SqlConnection(WebConfig.ConnectionString))
                {
                    _sqlConnection.Open();
                    DynamicParameters _mediumParameters = new DynamicParameters();
                    _mediumParameters.Add("@MediumId", _medium.MediumId);
                    _mediumParameters.Add("@MediumName", _medium.MediumName);
                    _mediumParameters.Add("@Description", _medium.Description);
                    _mediumParameters.Add("@IsActive", _medium.IsActive);

                    _mediumParameters.Add("@RowCount", 0, DbType.Int32, ParameterDirection.Output);

                    var result = _sqlConnection.Execute("uspUpdateMedium", _mediumParameters, commandType: CommandType.StoredProcedure);
                    _updateResult = _mediumParameters.Get<int>("RowCount");
                    _sqlConnection.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }
            return _updateResult;
        }

        public int ActivateDeactivateMedium(long _mediumId, bool _isActive)
        {
            int _updateResult = 0;
            try
            {
                using (SqlConnection _sqlConnection = new SqlConnection(WebConfig.ConnectionString))
                {
                    _sqlConnection.Open();
                    DynamicParameters _mediumParameters = new DynamicParameters();
                    _mediumParameters.Add("@MediumId", _mediumId);
                    _mediumParameters.Add("@IsActive", _isActive);

                    _mediumParameters.Add("@RowCount", 0, DbType.Int32, ParameterDirection.Output);

                    var result = _sqlConnection.Execute("uspActiveDeactivateMedium", _mediumParameters, commandType: CommandType.StoredProcedure);
                    _updateResult = _mediumParameters.Get<int>("RowCount");
                    _sqlConnection.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }
            return _updateResult;
        }
    }
}