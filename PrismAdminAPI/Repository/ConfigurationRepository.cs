﻿using Dapper;
using PrismAdminAPI.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace PrismAdminAPI.Repository
{
    public class ConfigurationRepository : IConfigurationRepository
    {
        public IEnumerable<StandardMedium> GetStandardMediamConfiguration(long? _standardMediumId = null)
        {
            IList<StandardMedium> _standardMediumList = new List<StandardMedium>();
            try
            {
                using (SqlConnection _sqlConnection = new SqlConnection(WebConfig.ConnectionString))
                {
                    _sqlConnection.Open();
                    DynamicParameters _standardParameters = new DynamicParameters();
                    _standardParameters.Add("@StandardMediumId", _standardMediumId);

                    _standardMediumList = _sqlConnection.Query<StandardMedium>("uspGetAllStandardMediumConfig", _standardParameters, commandType: CommandType.StoredProcedure).ToList();
                    _sqlConnection.Close();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return _standardMediumList;
        }

        public long InsertStandardMedium(StandardMedium _standardMedium)
        {
            long _addResult = 0;
            try
            {
                using (SqlConnection _sqlConnection = new SqlConnection(WebConfig.ConnectionString))
                {
                    _sqlConnection.Open();
                    DynamicParameters _standardmediumParameters = new DynamicParameters();
                    _standardmediumParameters.Add("@StandardId", _standardMedium.StandardId);
                    _standardmediumParameters.Add("@MediumId", _standardMedium.MediumId);
                    _standardmediumParameters.Add("@IsActive", _standardMedium.IsActive);
                    _standardmediumParameters.Add("@StandardMediumId", _standardMedium.StandardMediumId, DbType.Int64, ParameterDirection.Output);

                    var result = _sqlConnection.Execute("uspInsertUpdateStandardMediumConfig", _standardmediumParameters, commandType: CommandType.StoredProcedure);
                    _addResult = _standardmediumParameters.Get<long>("StandardMediumId");
                    _sqlConnection.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }
            return _addResult;
        }

        public IEnumerable<StandardMedium> GetConfiguredStandardMediamConfiguration(long? _standardId = null)
        {
            IList<StandardMedium> _standardMediumList = new List<StandardMedium>();
            try
            {
                using (SqlConnection _sqlConnection = new SqlConnection(WebConfig.ConnectionString))
                {
                    _sqlConnection.Open();
                    DynamicParameters _standardParameters = new DynamicParameters();
                    _standardParameters.Add("@StandardId", _standardId);

                    _standardMediumList = _sqlConnection.Query<StandardMedium>("uspGetConfiguredStandardMedium", _standardParameters, commandType: CommandType.StoredProcedure).ToList();
                    _sqlConnection.Close();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return _standardMediumList;
        }

    }
}