﻿using Dapper;
using PrismAdminAPI.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace PrismAdminAPI.Repository
{
    public class ChapterRepository : IChapterRepository
    {
        public IEnumerable<Chapter> GetChapterList(long? chapterId = null, bool? isActive = null)
        {
            IList<Chapter> _chapterList = new List<Chapter>();
            try
            {
                using (SqlConnection _sqlConnection = new SqlConnection(WebConfig.ConnectionString))
                {
                    _sqlConnection.Open();
                    DynamicParameters _chapterParameters = new DynamicParameters();
                    _chapterParameters.Add("@ChapterId", chapterId);
                    _chapterParameters.Add("@IsActive", isActive);

                    _chapterList = _sqlConnection.Query<Chapter>("uspGetAllChapters", _chapterParameters, commandType: CommandType.StoredProcedure).ToList();
                    _sqlConnection.Close();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return _chapterList;
        }

        public long InsertChapter(Chapter _chapter)
        {
            long _addResult = 0;
            try
            {
                using (SqlConnection _sqlConnection = new SqlConnection(WebConfig.ConnectionString))
                {
                    _sqlConnection.Open();
                    byte[] uploadedFile = null;
                    if (_chapter.ChapterThumbnail != null)
                    {
                        uploadedFile = new byte[_chapter.ChapterThumbnail.InputStream.Length];
                        _chapter.ChapterThumbnail.InputStream.Read(uploadedFile, 0, uploadedFile.Length);
                    }
                    DynamicParameters _chapterParameters = new DynamicParameters();
                    _chapterParameters.Add("@StandardId", _chapter.StandardId);
                    _chapterParameters.Add("@SubjectId", _chapter.SubjectId);
                    _chapterParameters.Add("@MediumId", _chapter.MediumId);
                    _chapterParameters.Add("@ChapterName", _chapter.ChapterName);
                    _chapterParameters.Add("@ChapterThumbnail", _chapter.ChapterThumbnail);
                    _chapterParameters.Add("@Description", _chapter.Description);
                    _chapterParameters.Add("@ChapterId", _chapter.ChapterId, DbType.Int64, ParameterDirection.Output);

                    var result = _sqlConnection.Execute("uspInsertChapter", _chapterParameters, commandType: CommandType.StoredProcedure);
                    _addResult = _chapterParameters.Get<long>("ChapterId");
                    _sqlConnection.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }
            return _addResult;
        }

        public int UpdateChapter(Chapter _chapter)
        {
            int _updateResult = 0;
            try
            {
                using (SqlConnection _sqlConnection = new SqlConnection(WebConfig.ConnectionString))
                {
                    _sqlConnection.Open();
                    DynamicParameters _chapterParameters = new DynamicParameters();
                    _chapterParameters.Add("@StandardId", _chapter.StandardId);
                    _chapterParameters.Add("@SubjectId", _chapter.SubjectId);
                    _chapterParameters.Add("@MediumId", _chapter.MediumId);
                    _chapterParameters.Add("@ChapterName", _chapter.ChapterName);
                    _chapterParameters.Add("@ChapterThumbnail", _chapter.ChapterThumbnail);
                    _chapterParameters.Add("@Description", _chapter.Description);
                    _chapterParameters.Add("@ChapterId", _chapter.ChapterId);
                    _chapterParameters.Add("@RowCount", 0, DbType.Int32, ParameterDirection.Output);

                    var result = _sqlConnection.Execute("uspUpdateChapter", _chapterParameters, commandType: CommandType.StoredProcedure);
                    _updateResult = _chapterParameters.Get<int>("RowCount");
                    _sqlConnection.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }
            return _updateResult;
        }

        public int ActivateDeactivateChapter(long _chapterId, bool _isActive)
        {
            int _updateResult = 0;
            try
            {
                using (SqlConnection _sqlConnection = new SqlConnection(WebConfig.ConnectionString))
                {
                    _sqlConnection.Open();
                    DynamicParameters _chapterParameters = new DynamicParameters();
                    _chapterParameters.Add("@StandardId", _chapterId);
                    _chapterParameters.Add("@IsActive", _isActive);

                    _chapterParameters.Add("@RowCount", 0, DbType.Int32, ParameterDirection.Output);

                    var result = _sqlConnection.Execute("uspActiveDeactivateChapter", _chapterParameters, commandType: CommandType.StoredProcedure);
                    _updateResult = _chapterParameters.Get<int>("RowCount");
                    _sqlConnection.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }
            return _updateResult;
        }
    }
}