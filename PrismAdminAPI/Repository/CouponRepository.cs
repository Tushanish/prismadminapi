﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Dapper;
using PrismAdminAPI.BAL;
using PrismAdminAPI.Models;

namespace PrismAdminAPI.Repository
{
    public class CouponRepository : ICouponRepository
    {
        private readonly IDbConnection connection = new SqlConnection(WebConfig.ConnectionString);

        public bool ActivateDeactivateCoupon(long? couponId, bool isActive)
        {
            int _updateResult = 0;
            try
            {
                using (connection)
                {
                    connection.Open();
                    DynamicParameters _couponParameters = new DynamicParameters();
                    _couponParameters.Add("@CouponId", couponId);
                    _couponParameters.Add("@IsActive", isActive);

                    _couponParameters.Add("@RowCount", 0, DbType.Int32, ParameterDirection.Output);

                    var result = connection.Execute("uspActiveDeactivateCoupon", _couponParameters, commandType: CommandType.StoredProcedure);
                    _updateResult = _couponParameters.Get<int>("RowCount");
                    connection.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }

            if (_updateResult > 0)
                return true;
            else
                return false;
        }

        public IEnumerable<Coupon> GetAllCoupons(long? couponId = null, bool? isActive = null)
        {
            IList<Coupon> _coupons = new List<Coupon>();
            try
            {
                using (SqlConnection _sqlConnection = new SqlConnection(WebConfig.ConnectionString))
                {
                    _sqlConnection.Open();
                    DynamicParameters _couponParameters = new DynamicParameters();
                    _couponParameters.Add("@CouponId", couponId);
                    _couponParameters.Add("@IsActive", isActive);

                    _coupons = _sqlConnection.Query<Coupon>("uspGetAllCoupons", _couponParameters, commandType: CommandType.StoredProcedure).ToList();
                    _sqlConnection.Close();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return _coupons;
        }

        public IEnumerable<LnkCouponDetails> GetAllLnkCouponDetails(long? couponId = null, long? sponsorerId = null)
        {
            IList<LnkCouponDetails> _lnkCoupons = new List<LnkCouponDetails>();
            try
            {
                using (connection)
                {
                    connection.Open();
                    DynamicParameters _couponParameters = new DynamicParameters();
                    _couponParameters.Add("@CouponId", couponId);
                    _couponParameters.Add("@SponsorerId", sponsorerId);

                    _lnkCoupons = connection.Query<LnkCouponDetails>("uspGetAllLnkCouponDetails", _couponParameters, commandType: CommandType.StoredProcedure).ToList();
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return _lnkCoupons;
        }

        public bool SaveBulkCoupon<T>(int batchSize, string tableName, List<T> _list)
        {
            using (var connection = new SqlConnection(WebConfig.ConnectionString))
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();

                using (var bulkCopy = new SqlBulkCopy(connection, SqlBulkCopyOptions.Default, transaction))
                {
                    bulkCopy.BatchSize = batchSize;
                    bulkCopy.DestinationTableName = tableName;
                    try
                    {
                        bulkCopy.WriteToServer(_list.AsDataTable());
                        
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        connection.Close();
                        return false;
                    }
                }

                transaction.Commit();
                return true;
            }
        }
    }
}