﻿using System.Collections.Generic;
using PrismAdminAPI.Models;

namespace PrismAdminAPI.Repository
{
    public interface ISponsorerRepository
    {
        bool AddSponsorer(Sponsorer sponsorer);
        bool ActivateDeactivateSponsorer(long SponsererId, bool IsActive);
        IEnumerable<Sponsorer> GetAll(long? sponsorerId = null, bool? isActive = null);
        bool UpdateSponsorer(Sponsorer sponsorer);
        bool AssignSponsorerToCoupon(long? fromCouponId, long? toCouponId, long? sponsorerId);
    }
}