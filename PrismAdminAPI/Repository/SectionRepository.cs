﻿using Dapper;
using PrismAdminAPI.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace PrismAdminAPI.Repository
{
    public class SectionRepository : ISectionRepository
    {
        public IEnumerable<Section> GetSectionList(long? _sectionId = null, bool? _isActive = null)
        {
            IList<Section> _sectionList = new List<Section>();
            try
            {
                using (SqlConnection _sqlConnection = new SqlConnection(WebConfig.ConnectionString))
                {
                    _sqlConnection.Open();
                    DynamicParameters _sectionParameters = new DynamicParameters();
                    _sectionParameters.Add("@SectionId", _sectionId);
                    _sectionParameters.Add("@IsActive", _isActive);


                    _sectionList = _sqlConnection.Query<Section>("uspGetAllSections", _sectionParameters, commandType: CommandType.StoredProcedure).ToList();
                    _sqlConnection.Close();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return _sectionList;
        }

        public long InsertSection(Section _section)
        {
            long _addResult = 0;
            try
            {
                using (SqlConnection _sqlConnection = new SqlConnection(WebConfig.ConnectionString))
                {
                    _sqlConnection.Open();
                    DynamicParameters _sectionParameters = new DynamicParameters();
                    _sectionParameters.Add("@SectionName", _section.SectionName);
                    _sectionParameters.Add("@Description", _section.Description);
                    _sectionParameters.Add("@IsStandardSection", _section.IsStandardSection);
                    _sectionParameters.Add("@IsChapterSection", _section.IsChapterSection);
                    _sectionParameters.Add("@SectionId", _section.SectionId, DbType.Int64, ParameterDirection.Output);

                    var result = _sqlConnection.Execute("uspInsertSection", _sectionParameters, commandType: CommandType.StoredProcedure);
                    _addResult = _sectionParameters.Get<long>("SectionId");
                    _sqlConnection.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }
            return _addResult;
        }

        public int UpdateSection(Section _section)
        {
            int _updateResult = 0;
            try
            {
                using (SqlConnection _sqlConnection = new SqlConnection(WebConfig.ConnectionString))
                {
                    _sqlConnection.Open();
                    DynamicParameters _sectionParameters = new DynamicParameters();
                    _sectionParameters.Add("@SectionId", _section.SectionId);
                    _sectionParameters.Add("@SectionName", _section.SectionName);
                    _sectionParameters.Add("@Description", _section.Description);
                    _sectionParameters.Add("@IsStandardSection", _section.IsStandardSection);
                    _sectionParameters.Add("@IsChapterSection", _section.IsChapterSection);
                    _sectionParameters.Add("@IsActive", _section.IsActive);

                    _sectionParameters.Add("@RowCount", 0, DbType.Int32, ParameterDirection.Output);

                    var result = _sqlConnection.Execute("uspUpdateSection", _sectionParameters, commandType: CommandType.StoredProcedure);
                    _updateResult = _sectionParameters.Get<int>("RowCount");
                    _sqlConnection.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }
            return _updateResult;
        }

        public int ActivateDeactivateSection(long _sectionId, bool _isActive)
        {
            int _updateResult = 0;
            try
            {
                using (SqlConnection _sqlConnection = new SqlConnection(WebConfig.ConnectionString))
                {
                    _sqlConnection.Open();
                    DynamicParameters _sectionParameters = new DynamicParameters();
                    _sectionParameters.Add("@SectionId", _sectionId);
                    _sectionParameters.Add("@IsActive", _isActive);

                    _sectionParameters.Add("@RowCount", 0, DbType.Int32, ParameterDirection.Output);

                    var result = _sqlConnection.Execute("uspActiveDeactivateSection", _sectionParameters, commandType: CommandType.StoredProcedure);
                    _updateResult = _sectionParameters.Get<int>("RowCount");
                    _sqlConnection.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }
            return _updateResult;
        }

        public int ActivateDeactivateStandardSection(long _sectionId, bool _isStandardSection)
        {
            int _updateResult = 0;
            try
            {
                using (SqlConnection _sqlConnection = new SqlConnection(WebConfig.ConnectionString))
                {
                    _sqlConnection.Open();
                    DynamicParameters _sectionParameters = new DynamicParameters();
                    _sectionParameters.Add("@SectionId", _sectionId);
                    _sectionParameters.Add("@IsStandardSection", _isStandardSection);

                    _sectionParameters.Add("@RowCount", 0, DbType.Int32, ParameterDirection.Output);

                    var result = _sqlConnection.Execute("uspActiveDeactivateStandardSection", _sectionParameters, commandType: CommandType.StoredProcedure);
                    _updateResult = _sectionParameters.Get<int>("RowCount");
                    _sqlConnection.Close();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return _updateResult;
        }


        public int ActivateDeactivateChapterSection(long _sectionId, bool _isChapterSection)
        {
            int _updateResult = 0;
            try
            {
                using (SqlConnection _sqlConnection = new SqlConnection(WebConfig.ConnectionString))
                {
                    _sqlConnection.Open();
                    DynamicParameters _sectionParameters = new DynamicParameters();
                    _sectionParameters.Add("@SectionId", _sectionId);
                    _sectionParameters.Add("@IsChapterSection", _isChapterSection);

                    _sectionParameters.Add("@RowCount", 0, DbType.Int32, ParameterDirection.Output);

                    var result = _sqlConnection.Execute("uspActiveDeactivateChapterSection", _sectionParameters, commandType: CommandType.StoredProcedure);
                    _updateResult = _sectionParameters.Get<int>("RowCount");
                    _sqlConnection.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }
            return _updateResult;
        }
    }
}