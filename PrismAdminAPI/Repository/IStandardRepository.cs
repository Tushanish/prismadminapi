﻿using PrismAdminAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrismAdminAPI.Repository
{
    public interface IStandardRepository
    {
        IEnumerable<Standard> GetStandardList(long? standardId = null, bool? isActive = null);

        long InsertStandard(Standard _standard);

        int UpdateStandard(Standard _standard);

        int ActivateDeactivateStandard(long _standardId, bool _isActive);
    }
}
