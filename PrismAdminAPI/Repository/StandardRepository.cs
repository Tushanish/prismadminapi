﻿using PrismAdminAPI.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Dapper;
using System.Data;

namespace PrismAdminAPI.Repository
{
    public class StandardRepository : IStandardRepository
    {
        
        public IEnumerable<Standard> GetStandardList(long? standardId=null,bool? isActive=null)
        {
            IList<Standard> _standardList = new List<Standard>();
            try
            {
                using (SqlConnection _sqlConnection = new SqlConnection(WebConfig.ConnectionString))
                {
                    _sqlConnection.Open();
                    DynamicParameters _standardParameters = new DynamicParameters();
                    _standardParameters.Add("@StandardId", standardId);
                    _standardParameters.Add("@IsActive", isActive);

                    _standardList = _sqlConnection.Query<Standard>("uspGetAllStandards", _standardParameters, commandType: CommandType.StoredProcedure).ToList();
                    _sqlConnection.Close();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return _standardList;
        }

        public long InsertStandard(Standard _standard)
        {
            long _addResult = 0;
            try
            {
                using (SqlConnection _sqlConnection = new SqlConnection(WebConfig.ConnectionString))
                {
                    _sqlConnection.Open();
                    DynamicParameters _standardParameters = new DynamicParameters();
                    _standardParameters.Add("@StandardName", _standard.StandardName);
                    _standardParameters.Add("@Description", _standard.Description);
                    _standardParameters.Add("@StandardId", _standard.StandardId,DbType.Int64,ParameterDirection.Output);

                    var result = _sqlConnection.Execute("uspInsertStandard", _standardParameters, commandType: CommandType.StoredProcedure);
                    _addResult= _standardParameters.Get<long>("StandardId");
                    _sqlConnection.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }
            return _addResult;
        }

        public int UpdateStandard(Standard _standard)
        {
            int _updateResult = 0;
            try
            {
                using (SqlConnection _sqlConnection = new SqlConnection(WebConfig.ConnectionString))
                {
                    _sqlConnection.Open();
                    DynamicParameters _standardParameters = new DynamicParameters();
                    _standardParameters.Add("@StandardId", _standard.StandardId);
                    _standardParameters.Add("@StandardName", _standard.StandardName);
                    _standardParameters.Add("@Description", _standard.Description);
                    _standardParameters.Add("@IsActive", _standard.IsActive);

                    _standardParameters.Add("@RowCount",0,DbType.Int32,ParameterDirection.Output);

                    var result = _sqlConnection.Execute("uspUpdateStandard", _standardParameters, commandType: CommandType.StoredProcedure);
                    _updateResult = _standardParameters.Get<int>("RowCount");
                    _sqlConnection.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }
            return _updateResult;
        }

        public int ActivateDeactivateStandard(long _standardId, bool _isActive)
        {
            int _updateResult = 0;
            try
            {
                using (SqlConnection _sqlConnection = new SqlConnection(WebConfig.ConnectionString))
                {
                    _sqlConnection.Open();
                    DynamicParameters _standardParameters = new DynamicParameters();
                    _standardParameters.Add("@StandardId", _standardId);
                    _standardParameters.Add("@IsActive", _isActive);

                    _standardParameters.Add("@RowCount", 0, DbType.Int32, ParameterDirection.Output);

                    var result = _sqlConnection.Execute("uspActiveDeactivateStandard", _standardParameters, commandType: CommandType.StoredProcedure);
                    _updateResult = _standardParameters.Get<int>("RowCount");
                    _sqlConnection.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }
            return _updateResult;
        }
    }
}