﻿using PrismAdminAPI.Models;
using PrismAdminAPI.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrismAdminAPI.Repository
{
    public interface ISubjectRepository
    {
        IEnumerable<SubjectViewModel> GetSubjectList(long? subjectId = null, bool? isActive = null);

        long InsertSubject(Subject _subject);

        int UpdateSubject(Subject _subject);

        int ActivateDeactivateSubject(long _subjectId, bool _isActive);
    }
}
