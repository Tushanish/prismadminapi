﻿using PrismAdminAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PrismAdminAPI.Repository
{
    public interface ICouponRepository
    {
        bool SaveBulkCoupon<T>(int batchSize, string tableName, List<T> _list);

        IEnumerable<Coupon> GetAllCoupons(long? couponId = null, bool? isActive = null);

        bool ActivateDeactivateCoupon(long? couponId, bool IsActive);

        IEnumerable<LnkCouponDetails> GetAllLnkCouponDetails(long? couponId = null, long? sponsorerId = null);
    }
}