﻿using Dapper;
using PrismAdminAPI.BAL;
using PrismAdminAPI.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace PrismAdminAPI.Repository
{
    public class SponsorerRepository : ISponsorerRepository
    {
        private readonly IDbConnection connection = new SqlConnection(WebConfig.ConnectionString);

        public bool AddSponsorer(Sponsorer sponsorer)
        {
            
            int _insertResult = 0;
            try
            {
                using (connection)
                {
                    connection.Open();
                    DynamicParameters _sponsorerParameters = new DynamicParameters();
                    _sponsorerParameters.Add("@SponsorerId", sponsorer.SponsorerId);
                    _sponsorerParameters.Add("@SponsorerName", sponsorer.SponsorerName);
                    _sponsorerParameters.Add("@ThumbnailImage", Helper.GetByteArray(sponsorer.ThumbnailImageFile));
                    _sponsorerParameters.Add("@LandingImage", Helper.GetByteArray(sponsorer.LandingImageFile));
                    _sponsorerParameters.Add("@IsActive", sponsorer.IsActive);
                    _sponsorerParameters.Add("@CreatedBy", sponsorer.CreatedBy);
                    _sponsorerParameters.Add("@ModifiedBy", sponsorer.ModifiedBy);

                    _sponsorerParameters.Add("@ReturnId", 0, DbType.Int32, ParameterDirection.Output);

                    var result = connection.Execute("uspInsertSponsorer", _sponsorerParameters, commandType: CommandType.StoredProcedure);
                    _insertResult = _sponsorerParameters.Get<int>("ReturnId");
                    connection.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }

            if (_insertResult > 0)
                return true;
            else
                return false;
        }

        public bool ActivateDeactivateSponsorer(long sponsererId, bool isActive)
        {
            int _updateResult = 0;
            try
            {
                using (connection)
                {
                    connection.Open();
                    DynamicParameters _sponsorerParameters = new DynamicParameters();
                    _sponsorerParameters.Add("@SponsorerId", sponsererId);
                    _sponsorerParameters.Add("@IsActive", isActive);

                    _sponsorerParameters.Add("@RowCount", 0, DbType.Int32, ParameterDirection.Output);

                    var result = connection.Execute("uspActiveDeactivateSponsorer", _sponsorerParameters, commandType: CommandType.StoredProcedure);
                    _updateResult = _sponsorerParameters.Get<int>("RowCount");
                    connection.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }

            if (_updateResult > 0)
                return true;
            else
                return false;
        }

        public IEnumerable<Sponsorer> GetAll(long? sponsorerId = null, bool? isActive = null)
        {
            IList<Sponsorer> _sponsorerList = new List<Sponsorer>();
            try
            {
                using (SqlConnection _sqlConnection = new SqlConnection(WebConfig.ConnectionString))
                {
                    _sqlConnection.Open();
                    DynamicParameters _sponsorerParameters = new DynamicParameters();
                    _sponsorerParameters.Add("@SponsorerId", sponsorerId);
                    _sponsorerParameters.Add("@IsActive", isActive);

                    _sponsorerList = _sqlConnection.Query<Sponsorer>("uspGetAllSponsorer", _sponsorerParameters, commandType: CommandType.StoredProcedure).ToList();
                    _sqlConnection.Close();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return _sponsorerList;
        }

        public bool UpdateSponsorer(Sponsorer sponsorer)
        {
            int _updateResult = 0;
            try
            {
                

                using (connection)
                {
                    connection.Open();
                    DynamicParameters _sponsorerParameters = new DynamicParameters();
                    _sponsorerParameters.Add("@SponsorerId", sponsorer.SponsorerId);
                    _sponsorerParameters.Add("@SponsorerName", sponsorer.SponsorerName);
                    _sponsorerParameters.Add("@ThumbnailImage", Helper.GetByteArray(sponsorer.ThumbnailImageFile));
                    _sponsorerParameters.Add("@LandingImage", Helper.GetByteArray(sponsorer.LandingImageFile));
                    _sponsorerParameters.Add("@IsActive", sponsorer.IsActive);
                    _sponsorerParameters.Add("@CreatedBy", sponsorer.CreatedBy);
                    _sponsorerParameters.Add("@ModifiedBy", sponsorer.ModifiedBy);

                    _sponsorerParameters.Add("@ReturnId", 0, DbType.Int32, ParameterDirection.Output);

                    var result = connection.Execute("uspUpdateSponsorer", _sponsorerParameters, commandType: CommandType.StoredProcedure);
                    _updateResult = _sponsorerParameters.Get<int>("ReturnId");
                    connection.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }

            if (_updateResult > 0)
                return true;
            else
                return false;
        }

        public bool AssignSponsorerToCoupon(long? fromCouponId, long? toCouponId, long? sponsorerId )
        {
            int _insertResult = 0;
            try
            {
                using (connection)
                {
                    connection.Open();
                    DynamicParameters _sponsorerParameters = new DynamicParameters();
                    _sponsorerParameters.Add("@FromCouponId", fromCouponId);
                    _sponsorerParameters.Add("@ToCouponId", toCouponId);
                    _sponsorerParameters.Add("@SponsorerId", sponsorerId);

                    _sponsorerParameters.Add("@RowCount", 0, DbType.Int32, ParameterDirection.Output);

                    var result = connection.Execute("uspInsertLinkSponsorerCoupon", _sponsorerParameters, commandType: CommandType.StoredProcedure);
                    _insertResult = _sponsorerParameters.Get<int>("RowCount");
                    connection.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }

            if (_insertResult > 0)
                return true;
            else
                return false;
        }
    }
}