﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PrismAdminAPI.Models
{
    public class Coupon
    {
        public long? CouponId { get; set; }
        public string CouponCode { get; set; }
        public bool  IsActive { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}