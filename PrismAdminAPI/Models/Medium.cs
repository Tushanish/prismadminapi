﻿using System;

namespace PrismAdminAPI.Models
{
    public class Medium
    {
        public int MediumId { get; set; }
        public string MediumName { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public long CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public long ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
    }
}