﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace PrismAdminAPI.Models
{
    public class Section
    {
        public long SectionId { get; set; }

        [DisplayName("Section Name")]
        [Required(ErrorMessage = "Section Name is required")]
        public string SectionName { get; set; }

        public string Description { get; set; }
        [DisplayName("For Standard")]
        public bool IsStandardSection { get; set; }
        [DisplayName("For Chapter")]
        public bool IsChapterSection { get; set; }

        public bool IsActive { get; set; }
        public long CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public long ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
    }
}