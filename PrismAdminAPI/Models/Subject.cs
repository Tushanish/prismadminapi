﻿using PrismAdminAPI.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;

namespace PrismAdminAPI.Models
{
    public class Subject
    {
        public long SubjectId { get; set; }

        [DisplayName("Subject Name")]
        [Required(ErrorMessage = "Subject Name is required")]
        public string SubjectName { get; set; }

        [Required(ErrorMessage = "Please select standard")]
        public long StandardId { get; set; }

        public long MediumId { get; set; }

        [DisplayName("Configured Mediums")]
        [Required(ErrorMessage = "Please select medium")]
        public string MediumIdList { get; set; }

        [DisplayName("Thumbnail Image")]
        public HttpPostedFileBase SubjectThumbnail { get; set; }

        public string Description { get; set; }
        
        public IEnumerable<SelectListItem> StandardList { get; set; }

        public bool IsActive { get; set; }
        public long CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public long ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }

        public SubjectViewModel SubjectViewModel { get; set; }
    }
}