﻿using System;
using System.Collections.Generic;

namespace PrismAdminAPI.Models
{
    public class SubjectDetails
    {
        public long SubjectDetailsId { get; set; }
        public long StandardId { get; set; }
        public long MediumId { get; set; }
        public long SubjectId { get; set; }
        public bool IsActive { get; set; }
        public long CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public long ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
    }
}