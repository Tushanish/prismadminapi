﻿using System;

namespace PrismAdminAPI.Models
{
    public class Student
    {
        public long StudentId { get; set; }
        public long LoginId { get; set; }
        public long StandardId { get; set; }
        public long MediumId { get; set; }
        public long CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public long ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
    }
}