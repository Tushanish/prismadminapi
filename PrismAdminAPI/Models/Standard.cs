﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace PrismAdminAPI.Models
{
    public class Standard
    {
        public long StandardId { get; set; }

        [DisplayName("Standard Name")]
        [Required(ErrorMessage ="Standard Name is required")]
        [Range(1, 15, ErrorMessage = "Please enter valid integer Number")]
        public string StandardName { get; set; }

        public string Description { get; set; }
        public bool IsActive { get; set; }
        public long CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public long ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
    }
}