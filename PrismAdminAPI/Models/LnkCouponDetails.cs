﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PrismAdminAPI.Models
{
    public class LnkCouponDetails
    {
        public int CouponDetailsId { get; set; }

        public int CouponId { get; set; }

        public int SponsorerId { get; set; }

        public List<SelectListItem> SponsorersList { get; set; } = new List<SelectListItem>();
    }
}