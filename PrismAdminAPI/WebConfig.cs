﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace PrismAdminAPI
{
    public class WebConfig
    {
        public static string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["prismConnectionString"].ConnectionString; }
        }

        public static string DefaultImage
        {
            get { return ConfigurationManager.AppSettings["DefaultImage"].ToString(); }
        }
    }
}